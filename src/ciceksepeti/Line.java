package ciceksepeti;


public class Line {
	
	double slope; 
	double a,b,c;
	double age;
	double angle;
	
	
	public Line(double a, double b, double c, double angle) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.angle = angle;
		this.slope = - a / b;
	}
	
	public String toString() {
		return a + "x + " + b + "y +" + c + "         angle: " + this.angle;
	}

}
