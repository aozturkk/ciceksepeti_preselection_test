package ciceksepeti;

public class Point {
	public double x;
	public double y;
	
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public String toString() {
		return "X: " + x + "     Y: " + y;
	}
	
	public Point subtract(Point gPoint) {
		return new Point(this.x - gPoint.x, this.y - gPoint.y);
	}
}
