package ciceksepeti;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class main {
	
	
	public static void main(String[] args) {
		
	//Bayii kordinatlar�n�n girdik 	
	Shop shopRed = new Shop("Shop Red",41.049792,29.003031); 
	Shop shopGreen = new Shop("Shop Green",41.069940,29.019250);
	Shop shopBlue = new Shop("Shop Blue",41.049997,29.026108);
	
	//Uc bayii nin a��rl�k noktas�n� bulduk
	Point gPoint = 	setGpoint(shopRed, shopGreen, shopBlue);
	
    //G noktas�ndan ve iki bayi ortas�ndan gececek sekilde 3 dogru olusturduk
	Line redGreen = generateLine(getPointRatio(shopRed.shopCoordinate,shopGreen.shopCoordinate,1,1).subtract(gPoint),new Point(0,0));
	Line greenBlue = generateLine(getPointRatio(shopBlue.shopCoordinate,shopGreen.shopCoordinate,1,1).subtract(gPoint),new Point(0,0));
	Line blueRed = generateLine(getPointRatio(shopRed.shopCoordinate,shopBlue.shopCoordinate,1,1).subtract(gPoint),new Point(0,0));


	//Kota ya gore dogrular�n ac�lar�n� optimize ettik (�deal algoritmazm�zda buras� otonomdur)	
	greenBlue.angle-=20;
	redGreen.angle+=10;
	blueRed.angle+=30;
	
	//siparisleri okuduk
	ArrayList <Order> orders = readOrders();	
	
	//Siparisleri bayilere g�re s�n�fland�rd�k
	orders.forEach(order -> 
				assignShop(order.orderId,order.orderCoordinate, gPoint, redGreen, greenBlue, blueRed,shopRed,shopGreen,shopBlue) 
		);
	
	
	//Bayiilere g�re siparis say�lar�n�n g�sterilmesi
	System.out.println("Red Shop :"+shopRed.orderBox.size());
	System.out.println("Green Shop :"+shopGreen.orderBox.size());
	System.out.println("Blue Shop :"+shopBlue.orderBox.size());
	
	//Bayiilere g�re siparis kordinatlar�n�n g�sterilmesi
	System.out.println("\nRed Shop :");
	shopRed.orderBox.forEach(order -> System.out.print("["+order.orderCoordinate.x +" , " +order.orderCoordinate.y+"] , " ));
	System.out.println("\n\nGreen Shop :");
	shopGreen.orderBox.forEach(order -> System.out.print("["+order.orderCoordinate.x +" , " +order.orderCoordinate.y+"] , " ));
	System.out.println("\n\nBlue Shop :");
	shopBlue.orderBox.forEach(order -> System.out.print("["+order.orderCoordinate.x +" , " +order.orderCoordinate.y+"] , " ));
	
	
	
	}
	
	//Teslimat adreslerinin g noktas�na g�re ac�lar�n�n bulunmas�
	public static double getAngle(Point p, Point gPoint) {
		return generateLine(p.subtract(gPoint),new Point(0,0)).angle;
	}
	
	//Teslimat adrelerini bayiilre g�re payla�t�rma fonksiyonu
	public static void assignShop (int orderId,Point p, Point gPoint, Line redGreen, Line greenBlue, Line blueRed,Shop shopRed,Shop shopGreen,Shop shopBlue) {
		double angle = getAngle(p,gPoint);
		if(greenBlue.angle >= angle) {
			shopGreen.orderBox.add(new Order(orderId,p.x,p.y));
			
		}
		else if(redGreen.angle < angle) {
			shopGreen.orderBox.add(new Order(orderId,p.x,p.y));
			
		}		
		else if(blueRed.angle < angle && redGreen.angle >= angle) {
			shopRed.orderBox.add(new Order(orderId,p.x,p.y));
			
			
		}
		else if(greenBlue.angle < angle && blueRed.angle >= angle) {
			shopBlue.orderBox.add(new Order(orderId,p.x,p.y));
			
		}
		
		
	}
	
	//Siparis okuma fonksiyonu
	public static ArrayList<Order> readOrders(){	
		String [] temp = new String[3];
		ArrayList <Order> orders = new ArrayList <Order>();
		File file=new File("siparis.txt");
		try {
			Scanner sc=new Scanner(file);
			
			while(sc.hasNextLine()){
				temp = sc.nextLine().split("\\s+");
				orders.add(new Order(Integer.parseInt(temp[0]),Double.parseDouble(temp[1]) ,Double.parseDouble(temp[2])));
			}
             sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return  orders;
	}
	
	//Uc Bayii nin orta noktas� (G point) bulan fonksiyon
	public static Point setGpoint(Shop a, Shop b, Shop c) {
	  double gPointX = (double) (a.shopCoordinate.x + b.shopCoordinate.x + c.shopCoordinate.x) / 3;
	  double gPointY = (double) (a.shopCoordinate.y + b.shopCoordinate.y + c.shopCoordinate.y) / 3;
		
		return new Point(gPointX, gPointY);
	}
	
	// iki bayii ras�ndaki uzakl�g� verilen oranlara g�re b�len fonksiyon
	public static Point getPointRatio(Point p1, Point p2, int ratio1, int ratio2) {
		
		double x = (((p2.x - p1.x) / (ratio1 + ratio2)) * ratio1) + p1.x; 
		double y = (((p2.y - p1.y) / (ratio1 + ratio2)) * ratio1) + p1.y; 
		
		return new Point(x,y);
	
	}
	
	//G noktas�ndan ve verilen noktadan gecen bir dogru olusturma
	public static Line generateLine(Point p, Point gPoint) {
		
		double m = (gPoint.y - p.y) / (gPoint.x - p.x);
		double a = m;
		double b = -1;
		double c = (-m*gPoint.x) + gPoint.y;
		double angle = (Math.toDegrees(Math.atan(m)));
		
		if(p.x < 0 && p.y > 0) {
			angle += 180;
		}
		else if(p.x < 0 && p.y < 0) {
			angle += 180;
		}
		else if(p.x > 0 && p.y < 0) {
			angle += 360;
		}
		
		return new Line(a,b,c, angle);
	}
	
	//Shop dogrular�n� bulma
	public static Line getShopLine(Shop s1, Shop s2, Point gPoint) {	
		Point shopMiddlePoint = getPointRatio(s1.shopCoordinate,s2.shopCoordinate,1,1);
		return generateLine(shopMiddlePoint,gPoint);	
	}
	
	
}
