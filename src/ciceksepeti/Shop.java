package ciceksepeti;

import java.util.ArrayList;

public class Shop {

	public String name;
	public Point shopCoordinate; 
	public ArrayList <Order> orderBox = new ArrayList <Order>();
	
	public Shop(String name,double x, double y) {
		this.name = name;
		this.shopCoordinate = new Point(x,y);
	}

}
